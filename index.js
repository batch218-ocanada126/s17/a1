//console.log("Hello World");
/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	 function printWelcomeData(){
		let fullName = prompt("Enter your full name: ");
	 	let age = prompt("Enter your age: ");
	 	let address = prompt("Enter your address: ");

	 	console.log("Hello, "+ fullName);
	 	console.log("Your are "+ age +" "+"years old" );
	 	console.log("You live in "+ address);
		
	 }

	 printWelcomeData();
/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function showBandsName(){
		let band1 = "1. Silent Sanctuary";
		let band2 = "2. Parokya ni Edgar";
		let band3 = "3. Secondhand Seranade";
		let band4 = "4. The Script";
		let band5 = "5. SpongeCola";

		console.log(band1);
		console.log(band2);
		console.log(band3);
		console.log(band4);
		console.log(band5);
	}

	showBandsName();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function showMovies(){
		let movie1 = "1. AI";
		let movie2 = "2. Jurassic World Dominion 2022";
		let movie3 = "3. Inception";
		let movie4 = "4. Avengers: Endgame";
		let movie5 = "5. Your Name (Kimi No Nawa)";

		console.log(movie1);
		console.log("Rotten Tomatoes Rating: 75%");
		console.log(movie2);
		console.log("Rotten Tomatoes Rating: 29%");
		console.log(movie3);
		console.log("Rotten Tomatoes Rating: 87%");
		console.log(movie4);
		console.log("Rotten Tomatoes Rating: 94%");
		console.log(movie5);
		console.log("Rotten Tomatoes Rating: 94%");
	}

	showMovies();
/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers();
function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);